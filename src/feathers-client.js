import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'
import auth from '@feathersjs/authentication-client'
import io from 'socket.io-client'

const socket = io('http://localhost:3030', {transports: ['websocket']})

const feathersClient = feathers()
  .configure(socketio(socket))
  //Electron throws error that window is not existing, replace with storage from cookie-storage
  //.configure(auth({ storage: window.localStorage }))

export default feathersClient